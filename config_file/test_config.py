"""
Simple example program of using a config file in:

    ~/.config/{PROGNAME}/config.dat

If the config file doesn't exist it is created from default data.

The config data is changed (set "time" and "date" values
to current) and written back to the config file.

The config file is in JSON format, but this can be changed.
"""

import os.path
import json
import datetime


# define the program and config file names
PROGNAME = 'test_config'
CONFIGFILENAME = 'config.dat'

# create full paths to config directory and the config file
ConfigDir = os.path.expanduser(os.path.join('~', '.config', PROGNAME))
ConfigFile = os.path.join(ConfigDir, CONFIGFILENAME)

DefaultConfig = {'date': '0000-00-00', 'time': '00:00:00'}


def get_config(default):
    """Return a config dictionary read from the config file.

    If the config file is not found, the "default" dictionary is returned.
    """

    try:
        with open(ConfigFile) as fd:
            data = fd.read()
        config = json.loads(data)
    except json.JSONDecodeError:
        # JSON file problem, just report to user
        print('*' * 60)
        print(f"* Config file '{ConfigFile}' is corrupt.")
        print(f"* Fix that or delete the file.")
        print('*' * 60)
        raise
    except FileNotFoundError:
        # file not there, return default
        # put_config(default)   # create the file, if required
        config = default

    return config


def put_config(config):
    """Write a config directory to the program's config file."""

    # create the *directory* if it's not there
    if not os.path.isdir(ConfigDir):
        os.makedirs(ConfigDir)

    # write the new config file
    print(config)
    with open(ConfigFile, 'w') as fd:
        fd.write(json.dumps(config))


# get the current config information
config = get_config(DefaultConfig)
print(f"Config file opened, {config['date']=}, {config['time']=}")

# update the config dictionary with the current date & time
now = datetime.datetime.now()
config['date'] = now.strftime("%Y-%m-%d")
config['time'] = now.strftime("%H:%M:%S")
print(f"Changing config['date']->{config['date']}, config['time']->{config['time']}")

# store changed config data in the config file
put_config(config)
