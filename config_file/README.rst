Config file
===========

A simple example of using a "configuration" file stored in the usual
directory:

    ~/.config/<program name>/

This is about as simple as you can get using a separate file.  To see a
method that uses an imported "common" module with autosave see the 
*global_data* tip.

Requirements
------------

We want the code to restore a "configuration" dictionary from a known place.
If the file doesn't exist the configuration defaults to set values.

There should be some method to update the configuration file with changed
values, if required.

Implementation
--------------

The configuration data is stored in a dictionary, though this could be changed
to other data structures, such as a named tuple, and so on.  The main
requirement is that the config object be able to be serialized into JSON.

Using named tuples as a config object means we can't change config information.
