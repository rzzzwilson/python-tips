# Convert integer into 8 bit binary string

This came up on reddit when a qustion was asked about getting the  underlying
bitfield for an integer.  The asker wanted to see the actual bits to understand
how binary operations like *~* affected the bits.  The answer was that in
modern python there probably isn't an underlying bitfield because `int`s
are implemented as "bignums".  But it was suggested that it was possible to 
write a function that would convert small integers into a corresponding
bitfield of 8 bits that might help with understanding binary bit operations.
The code here is that function.
