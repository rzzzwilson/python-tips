def int2bin(n):
    """Convert "n" to an 8 bit twos-complement binary string."""
    
    if not -129 < n < 128:
        raise ValueError(f"{n} is out of range.")
    
    # get the absolute value binary string
    bin_str = f"{abs(n):08b}"
      
    # if we have a negative number -> twos-complement
    if n < 0:
        # flip all bits of the string, create list of ints
        result = []
        for ch in bin_str:
            result.append(0 if ch=="1" else 1)
        
        # add 1 to rightmost end, carry to left, each bit left as a string
        carry = 1
        for i in range(7, -1, -1):
            val = result[i] + carry
            if val > 1:
                carry = 1
                val = 0
            else:
                carry = 0
            
            result[i] = str(val)
            
        bin_str = "".join(result)
                
    return bin_str
        
for x in range(-5, 6):
    print(f"x={x:2d}, x={int2bin(x)}, ~x={int2bin(~x)}, x&6={int2bin(x&6)}, x|9={int2bin(x|9)}")

# expect exception when "x" goes out of range
for x in range(0, 200):
    print(f"{x=:2d} {int2bin(x)=}")

