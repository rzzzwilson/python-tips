#!/bin/env python

"""
This is a python CLI program template.

Usage: template_cli.py [-h] [-d <debug>]

where  -h          prints this help and stops
       -d <debug>  sets the level of debug to <debug> (default 10)
"""

import sys
import getopt
import traceback


# the main code goes here
def main(debug):
    print(f"main() called, {debug=}")
    return 0

# to help the befuddled user
def usage(msg=None):
    if msg:
        print(f"{'*'*80}\n{msg}\n{'*'*80}")
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = f"\n{'=' * 80}"
    msg += "\nUncaught exception:\n"
    msg += "".join(traceback.format_exception(type, value, tb))
    msg += f"{'=' * 80}\n"
    print(msg)    # usually we would log the exception

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]
try:
    (opts, args) = getopt.getopt(argv, "d:h", ["debug=", "help"])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

debug = 10              # no logging

for (opt, param) in opts:
    if opt in ["-d", "--debug"]:
        if not param.isnumeric():
            usage("The debug level must be an integer.")
            sys.exit(1)
        debug = int(param)
    elif opt in ["-h", "--help"]:
        usage()
        sys.exit(0)

# run the program code
result = main(debug)
sys.exit(result)
