"""
The 'questions' data is a list of lists.  Each sublist has this form:

    [print, question, yesindex, noindex]

It contains all data for one possible question.

If the <print> string is not empty print the string before asking
the associated questioni, if any.

The <question> string is a question to be asked (if not empty) and
a yes/no answer is expected.

The <yesindex> and <noindex> values are indices of the next question if
the answer to the question was yes or no respectively.  If the chosen index
is `None` the questions are at an end.  If the <question> string is empty
no question is asked and the <yesindex> is assumed.

The "question" data structure should probably be created programmatically.
"""

questions = [["", "Does it work?", 10, 1],                                          # 0
             ["", "Did you mess with it?", 2, 7],                                   # 1
             ["The headache starts. Try to fix it.", "Did you fix it?", 11, 7],     # 2
             ["You're in trouble!", "Does anybody know?", 4, 5],                    # 3
             ["", "Can you blame somebody else?", 11, 12],                          # 4
             ["", "Can you cover it up?", 11, 6],                                   # 5
             ["", "Can you blame anyone else?", 11, None],                          # 6
             ["", "Will you pay for it?", 12, 8],                                   # 7
             ["", "Will you be fired?", 12, 9],                                     # 8
             ["Forget it", "", None, None],                                         # 9
             ["Leave it alone!", "", 11, None],                                     # 10
             ["No problem", "", None, None],                                        # 11
             ["You're screwed lmao", "", None, None],                               # 12
            ]


def ask_yes_no(prompt):
    """Ask the qustion in prompt and returns True/False
    depending on whether the response is yes/no.

    If not recognized as yesy or no, ask again.
    """

    if not prompt.endswith(" "):
        prompt += " "

    while True:
        response = input(prompt).lower()
        response = response[:1]     # get just the first character, if any
        if response in {"y", "n"}:
            return response == "y"
        print("Sorry, only YES or NO answers allowed.  Try again.")

def ask_questions(qlist, index=0):
    """Ask questions in the "qlist" lists.

    index  is the index of the question to ask
    """

    while index is not None:
        [pstr, qstr, yindex, nindex] = qlist[index]

        if pstr:
            print(pstr)

        response = True
        index = None

        if qstr:
            response = ask_yes_no(qstr)
            index = yindex if response else nindex

ask_questions(questions)
