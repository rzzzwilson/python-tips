# How to save data in a python file

This is a little python hack that allows you to save textual data in
the file you are directly executing.  The basic idea is to save some data
at the end of the python file.  These data lines must start with the "#"
comment character so as not to get a syntax error on the next execution
of the file.  To make identification of the data lines easier a special
delimiter line is added to the start of the data.

This code saves the data as a JSON string.  The data stored is the
"accession" number and the date+time the file was updated.

## Testing

On the first execution of a "clean" test file, the code just prints:

    No saved data found

and the code file has new data written at its end.  Check this by looking
in the file.  On executing the file twice more we see this printed:

    $ python test.py
    Saved accession was 1, time was Sun May 19 09:02:46 2024
    $ python test.py
    Saved accession was 2, time was Sun May 19 09:03:53 2024

Looking in the file shows the updated data.


## Notes

The `__file__` variable contains the absolute path to the file being executed,
making is easy to find the file of interest.
