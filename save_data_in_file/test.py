"""
Test code to see if we can store data at the bottom
of the python file we are executing.

Just store the "accession number" and date+time the code ran.
"""

import time
import json


def process(lines):
    """Process JSON data in a list of lines."""

    # remove leading "#" and trailing \n
    clean = [l.strip()[2:] for l in lines]

    # decode the JSON
    return json.loads(" ".join(clean))


# the code line after which we save data
delim = "# ~~~~~ dynamic data below ~~~~~\n"


# get all code lines in the file
with open(__file__) as fd:
    code = fd.readlines()

# if there is a delimiter, extract data
# and remove the delimiter and everything after it
# then process the data
accession = 0

if delim in code:
    # process saved data
    index = code.index(delim)
    data = code[index+1:]
    (accession, when) = process(data)
    print(f"Saved accession was {accession}, time was {when}")

    # remove all the dynamic data
    if code[index-1] == "\n":   # remove any prior blank line
        index -= 1
    code = code[:index]
else:
    print("No saved data found")

# add the new delimiter plus saved data, rewrite code file
code.append("\n")
code.append(delim)
data = json.dumps((accession+1, time.asctime()))
code.append("# " + data + "\n")
with open(__file__, "w") as fd:
    fd.write("".join(code))
