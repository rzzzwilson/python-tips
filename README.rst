Python tips
=========== 

Little bits and pieces to solve small python problems.

save_data_in_file
-----------------

A simple example showing how to save data at the end of the python
file you are directly executing.

global_data
-----------

A method of using global data persistent in a file.

Allows multiple modules to share data that is defined in a data file.
The data file can be updated with changes made during execution.

raw_keyboard
------------

A function to return the value of a keypress **without** pressing ENTER.

Used:

    from getch import getch
    
    ch = getch()
    print(f"0x{ord(ch):02X}\n'{ch}'")

also has code for a *timed* input function.

replace_func_code
-----------------

Code to show how to replace the executable code of a function with
new code.

input_field
-----------

An example of how to get user input in the *middle* of a prompt string.
