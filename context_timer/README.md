# A context manager to measure elapsed time

Experimental code to see if we can use a context manager to measure
the time it took to execute a body of code.  Something like this:

    with ElapsedTime as timer:
        # do something here
    print(f"took {timer.elapsed_time:.2f}s")

There are a couple of ways to write a context manager:

* as a class
* as a generator

We test both.


## A class-based timer

We can do this:

    from time import perf_counter, sleep
    
    class ElapsedTime:
        def __init__(self):
            pass
        def __enter__(self):
            self._start_time = perf_counter()
            return self
        def __exit__(self, type, value, traceback):
            self.elapsed_time = perf_counter() - self._start_time
    
    with ElapsedTime() as e:
        sleep(0.2)
    print(f"overall took {e.elapsed_time:.2f}s")

This depends on the context manager object (`e` above) existing **after**
the managed code finishes.  The sample code in *class.py* tests nesting
of context managers, as well as adding convenience methods like *__str__()*
that allows direct printing of the context manager object.


## A generator-based timer

We have this in *generator.py*:

    import time
    from contextlib import contextmanager
    
    @contextmanager
    def ElapsedTime():
        start = time.time()
        try:
            yield start
        finally:
            ElapsedTime.elapsed_time = time.time() - start
    
    with ElapsedTime():
        time.sleep(.1)
    print(f"took {ElapsedTime.elapsed_time:.2f}s")

This code uses a *function attribute* which is allowed in python, but
isn't something that's done often.  Using that also means we can't nest
context manager usages, which may matter.

We could make the function attribute be a list of elapsed times with the
rightmost being the most recent elapsed time, but that's messy to use
and prone to error if the elapsed times aren't immediately used.


# Summary

The class-based timer is the most flexible since the generator-based
timer can't be nicely nested.  Additionally, the class-based approach
has convenience methods like *__str__()* and *__float()__* that make
handling the elapsed time more convenient.
