#!/usr/bin/env python

"""
Experimental code to measure elapsed time with a context manager
implemented as a generator.
"""

import time
from contextlib import contextmanager

@contextmanager
def ElapsedTime():
    start = time.time()
    try:
        yield start
    finally:
        ElapsedTime.elapsed_time = time.time() - start

with ElapsedTime():
    time.sleep(.1)
print(f"took {ElapsedTime.elapsed_time:.2f}s")
