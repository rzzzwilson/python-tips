"""
Experimental code to measure elapsed time with a context manager
implemented as a class.
"""

from time import perf_counter, sleep

class ElapsedTime:
    def __enter__(self):
        self.time = perf_counter()
        return self
    def __exit__(self, type, value, traceback):
        self.time = perf_counter() - self.time
    def __float__(self):
        return float(self.time)
    def __int__(self):
        return int(self.time)
    def __str__(self):
        return str(float(self.time))
    def __repr__(self):
        return str(float(self.time))

# test nested context managers
with ElapsedTime() as e:
    with ElapsedTime() as e1:
        sleep(.01)
    print(f"first took {e1.time:.2f}s")
    with ElapsedTime() as e2:
        sleep(.02)
    print(f"second took {float(e2):.2f}s")
print(f"overall took {e.time:.2f}s")
print(f"{e=}")
print(f"{str(e)=}")
print(f"{float(e)=}")
print(f"{int(e)=}")
