"""
Quicksort in python.

Inspired by this Computerphile video:
    
    https://m.youtube.com/watch?v=OKc2hAmMOY4
"""

def qsort(data):
    """Return a sorted list of values from sequence "data"."""
    
    if not data:
        return data

    (*rest, pivot) = data
    lesseq = [x for x in rest if x <= pivot]
    greater = [x for x in rest if x > pivot]

    return qsort(lesseq) + [pivot] + qsort(greater)
    
###########################################
# some test code
###########################################

import random

data = list(range(20))
random.shuffle(data)

print(data)
print(qsort(data))

