A small code sample showing the basic quicksort algorithm.

The basic idea was seen in this Computerphile video:
    
    https://m.youtube.com/watch?v=OKc2hAmMOY4

Just execute the *qsort.py* file to see the sort in action.

Production-ready quicksort code is a *LOT* more complicated.
