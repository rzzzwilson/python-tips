This code came out of trying to answer this reddit post:

    https://old.reddit.com/r/learnpython/comments/1j3qa66/im_wondering_if_it_is_possible_to_make_a/

Initially, I just wrote code to draw a button wherever the user left-clicked
the mouse.  That quickly morphed into a more general system that can handle
more than one button and also respond to clicks within the button.

Just execute the code.  Right mouse click where you want a new button
to be drawn.  Left click within any button to call code that prints a response.

The code here is quite simple and doesn't handle some problems/oversights.  
Things that could be improved:

* speed of button drawing
* add optional fill color
* draw nicer button outlines, rounded corners, etc
* font size of button text
* better buton text centreing
* better state save, turtle visibility, etc
* make more general, handle many turtles, etc
