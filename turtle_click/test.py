import turtle as t

def push_state():
    """Save the given turtle state onto a stack.

    Saves the turtle:
        position
        heading
        up/down state
    """

    turtle_state.append((t.pos(), t.heading(), t.pen()))

def pop_state():
    """Restore the turtle state to the previous save."""

    (pos, head, pen) = turtle_state.pop()
    t.penup()       # can't get teleport()
    t.goto(*pos)    # to work, workaround
    t.seth(head)
    t.pen(pen)

def draw_button(x, y, w, h, text=""):
    """Draw a button at (x,y) with given width, height and text."""

    # save state before starting
    push_state()

    # draw button outline
    # could make this much more fancy,
    #     rounded corners, fill colour, etc
    t.penup()
    t.goto(x, y)
    t.seth(0)
    t.pendown()
    t.forward(w)
    t.right(90)
    t.forward(h)
    t.right(90)
    t.forward(w)
    t.right(90)
    t.forward(h)

    # draw button text
    t.penup()
    t.goto(x+w/2, y-h/2 - 7)    # "7" is fudge factor, edit as required
    t.write(text, align="center")

    # remember the draw button for when we left click
    save = (x, y, w, h, text)
    buttons.append((x, y, w, h, text))

    # restore state and return
    pop_state()

def make_button(x, y):
    global button_num

    button_num += 1
    draw_button(x, y, 100, 40, f"button{button_num}")

def check_button(x, y):
    for (bx, by, bw, bh, btext) in buttons:
        if (bx < x < bx + bw) and (by-bh < y < by):
            print(f"Button {btext} was pushed.")
#            return  # don't handle overlapping buttons

turtle_state = []
button_num = 0
buttons = []        # remember the buttons drawn

t.degrees()
t.onscreenclick(make_button, 3)     # draw when right mouse button clicked
t.onscreenclick(check_button, 1)    # see if left click on button
t.mainloop()
