"""
Try to implement something like the C language __LINE__ feature.

Usage will be something like:

    from __line__ import __LINE__
    print(f"{__LINE__}: line")
"""

import inspect

class LineNo:
    def __repr__(self):
        return str(inspect.currentframe().f_back.f_lineno)

__LINE__ = LineNo()
