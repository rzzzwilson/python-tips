__line__
========

A bit of code to provide something like the C language __LINE__  macro
feature.  We want to be able to do::

    from __line__ import __LINE__
    print(f"{__LINE__}: some text")

and have this printed::

    2: some text

Note: you must convert the __LINE__ thing to a string on the line of interest.
So you need to do at least this:

    print(str(__LINE__))
