Keyboard
========

A context manager to return the value of a keypress WITHOUT pressing ENTER.
The code does not wait for a keypress, it returns None if no key pressed.

Used::

    from raw_keyboard import Keyboard

    with Keyboard() as getch:
        ch = getch()
        if ch is not None:
            print(f"0x{ord(ch):02X}\n'{ch}'")

Usage
-----

This small module is designed to be imported.  But you can test the operation
by executing the `raw_keyboard.py` file from the command line::

    $ python3 raw_keyboard.py 
    Press ESCAPE twice to exit
    'q', 0x71
    'w', 0x77
    'e', 0x65
    ', 0x1b
    ', 0x1b
    $

While the program is waiting it "twirls" a little activity marker so you know
it's not hanging on a keypress.

Alternatively
-------------

You don't have to use the context manager.  You could call the Keyboard()
methods directly.  The example file *test.py* shows how.  If you do this you
should pass `cleanup=True` to the constructor.  This registers an `atexit`
handler to clean up for you.

Alternatively Alternatively
---------------------------

The code above returns None if no key was pressed.  That is, the code doesn't
block.  If you don't mind blocking calls you can use the code in the *getch.py*
module.

Timed Input
-----------

As an example of how to use the context manager, 
the file `timed_input.py` contains the *timed_input()* function::

    timed_input(timeout, prompt='')
    
    where timeout is a float timeout value in seconds
          prompt  is the prompt string to print

The function returns the input string, including the NEWLINE, if
the timeout is not exceeded, else it returns None.

Doesn't allow inline editing, ie, arrow keys or backspace.

sleep_ignore.py
---------------

This example module shows how to ignore typed input during a
*time.sleep()*.  Just run the code to test it.  Your code would normally
do something like this::

    from sleep_ignore import sleep_ignore
    
    print("Some text.")
    sleep_ignore(5)     # sleep for 5 seconds, ignore input
    print("After sleeping.")
