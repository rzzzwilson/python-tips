"""
Example showing how to call raw_keyboard code without using the context manager.
"""

from raw_keyboard import Keyboard

kb = Keyboard(cleanup=True)

print("This program echoes every keystroke received as a hex value.")
print("Press ESC twice to exit.")

esc_count = 0

while True:
    ch = kb.getch()
    if ch is None:
        continue        # no key was pressed
    print(f"0x{ord(ch):02X}")
    if ord(ch) == 0x1B:
        esc_count += 1
    else:
        esc_count = 0
    if esc_count >= 2:
        break
