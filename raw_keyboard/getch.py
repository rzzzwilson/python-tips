"""
A function to return the value of a keypress WITHOUT pressing ENTER.
This code DOES wait for the keypress.

Used:

    from getch import getch

    ch = getch()
    print(f"0x{ord(ch):02X}\n'{ch}'")

"""

import sys

try:
    import msvcrt

    def getch():
        """Return a "raw" keypress without needing ENTER."""

        return msvcrt.getch()
except ImportError:
    try:
        import tty, termios

        def getch():
            """Return a "raw" keypress without needing ENTER."""

            fd = sys.stdin.fileno()
            old_settings = termios.tcgetattr(fd)
            try:
                tty.setraw(sys.stdin.fileno())
                ch = sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
            return ch
    except ImportError:
        print(f"Sorry, no getch() available for sys.platform='{sys.platform}'")
        sys.exit(1)

if __name__ == '__main__':
    # The "esc" character (0x1B) will be displayed as "⦻". 
    # Two escapes in a row will terminate the loop.

    ESCAPE_DISPLAY = '⦻'
    esc_count = 0

    while True:
        ch = getch()
        if ord(ch) == 0x1B:
            esc_count += 1
            print(f"0x{ord(ch):02X}\t'{ESCAPE_DISPLAY}'")
        else:
            esc_count = 0
            print(f"0x{ord(ch):02X}\t'{ch}'")
        if esc_count >= 2:
            break

