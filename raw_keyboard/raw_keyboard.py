#!/usr/bin/env python

"""
A Python context manager to get keypresses without needing ENTER.
The context function returns None if no key was pressed.
Should work on Linux, MacOS and Windows, but only tested on Linux
and MacOS.

    with Keyboard() as getch:
        key = getch()
        if key is not None:
            print(f'Got key {key:02x}')

Original idea from:
http://stackoverflow.com/questions/2408560/python-nonblocking-console-input
"""

import os

if os.name == 'nt':
    # Windows
    import msvcrt
else:
    # Posix (Linux, OS X)
    import sys
    import atexit
    import termios
    from select import select


class Keyboard:
    def __init__(self, cleanup=False):
        """Creates a Keyboard object that you can call to get any key pressed.

        cleanup  if True, will automatically register the self.reset() method
        """

        if os.name == 'nt':
            pass
        else:
            # Save the terminal settings
            self.fd = sys.stdin.fileno()
            self.new_term = termios.tcgetattr(self.fd)
            self.old_term = termios.tcgetattr(self.fd)

            # New terminal setting unbuffered
            self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
            termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)

            if cleanup:
                atexit.register(self.reset)

    def reset(self):
        """ Resets to normal terminal.  On Windows this is a no-op."""

        if os.name == 'nt':
            pass
        else:
            # restore the original environment
            termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)

    def getch(self):
        """Returns a keyboard character if select() says key was pressed.

        Returns None if no key pressed.
        """

        if os.name == 'nt':
            if msvcrt.kbhit():
                return msvcrt.getch().decode('utf-8')
        else:
            if select([sys.stdin], [], [], 0) == ([sys.stdin], [], []):
                return sys.stdin.read(1)
        return None

    def __enter__(self):
        return self.getch

    def __exit__(self, ex_type, ex_value, ex_traceback):
        self.reset()

# code to exercise the Keyboard() context manager

if __name__ == "__main__":
    # data for the "backwards twirling" indicator
    twirl = '|\\-/'
    twirl_len = len(twirl)
    twirl_index = 0 

    # set the escape counter
    esc_count = 0

    print('Press ESCAPE twice to exit')
    with Keyboard() as getch:
        while True:
            # show the updated "twirl"
            print(f'{twirl[twirl_index]}\r', end='')
            twirl_index += 1
            if twirl_index >= twirl_len:
                twirl_index = 0
    
            # get keypress, if any
            c = getch()
            if c is None:
                # continue loop if no keypress
                continue
    
            # show the character value
            print(f"'{c}', 0x{ord(c):02x}")

            # abort if ESC pressed, maybe
            if ord(c) == 0x1B:
                # it's ESC!
                esc_count += 1
                if esc_count >= 2:
                    # two in a row, quit
                    print('  \r', end='')         # remove the twirl
                    break
            else:
                esc_count = 0
