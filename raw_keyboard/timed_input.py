"""
Code to test feasability of having a "timed" input function that
will only accept input up to a certain time limit.

Usage:  timed_input(timeout, prompt='')

where timeout is a float timeout value in seconds
      prompt  is the prompt string to print

The function returns the input string, including the \n, if
the timeout is not exceeded, else it returns None.

Should handle backspace/delete key, but won't handle inline editing.
"""

import time
import platform
from raw_keyboard import Keyboard

# set value of "backspace" key by operating system
OS = platform.system()
if OS == 'Linux':
    Backspace = '\b'
elif OS == 'Darwin':
    Backspace = chr(0x7F)   # MacOS has only "delete" key
elif OS == 'Windows':
    Backspace = '\b'
else:
    raise RuntimeError("Can't handle '{OS}' operating system")

def timed_input(timeout, prompt=''):
    # calculate the end time and print the prompt string
    end_time = time.time() + timeout
    print(prompt, end='', flush=True)

    # prepare the result list, which collects keys
    result = []

    # read keyboard, wait for RETURN
    with Keyboard() as getch:
        while time.time() < end_time:
            ch = getch()        # doesn't wait, returns None if no keypress
            if ch is None:
                continue
            if ch == Backspace: # user wants to delete last char, if any
                if result:
                    print('\b \b', end='', flush=True)  # remove last char on screen
                    result.pop()
                continue
            if ch == '\n':      # end of input, return result
                print()
                result.append('\n')
                return ''.join(result)
            print(ch, end='', flush=True)
            result.append(ch)

        # timeout, just return None
        print()
        return None

if __name__ == '__main__':
    Timeout = 10
    result = timed_input(Timeout, f'timed prompt ({Timeout}s): ')
    if result is not None:
        print(f"len(result)={len(result)}, result='{result}'")
    else:
        print('No input')
