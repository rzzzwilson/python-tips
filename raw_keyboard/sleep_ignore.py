"""
Example showing how to ignore typed text during a time.sleep().

A CTRL-C will terminate the program.  Use a try/except to catch
that exception and terminate the sleep().
"""

import time
from raw_keyboard import Keyboard


def sleep_ignore(sleep_time):
    """Perform a time.sleep() ignoring user type input."""

    with Keyboard() as getch:
        time.sleep(sleep_time)

        # gobble up anything that was typed
        while getch() is not None:
            pass

if __name__ == "__main__":
     print(f"Sleeping for 5 seconds, type anything during that time.")
     sleep_ignore(5)

     text = input("Type some text: ")
     print(f"You typed: '{text}'.")
     print("Whatever you typed while waiting was ignored.")
