"""
A program to draw the path of the "trapped knight".

https://www.youtube.com/watch?v=RGQe8waGJ4w
"""

import sys
import getopt
import traceback
import itertools

# max number needed to fill board past the "trapped knight" number (2084)
MaxNumber = 10000

# directions defining the "sense" of the spiral
Directions = [(1,0), (0,1), (-1,0), (0,-1)]
#               E      N       W      S

# path to the output data file
OutputData = 'data.out'


def dirn_gen():
    """Generator to produce 'E', 'N', 'W', 'S', 'E', ..."""

    for i in itertools.cycle(Directions):
        yield i

def best_move(x, y, board, marked):
    """Returns best possible (a,b) moves from (x,y) position.

    x, y   current position
    board  dict holding (x,y): num
    marked  set holding (x,y) already marked

    Returns None if no possible move.
    """

    result = []

    # cycle through all possible moves, check squares already visited
    for (dx, dy) in [(+2,+1), (+2,-1), (+1,+2), (-1,+2), (-2,+1), (-2,-1), (+1,-2), (-1,-2)]:
        (new_x, new_y) = (x+dx, y+dy)
        if (new_x, new_y) not in marked:
            # add (number, posn) to "result"
            result.append((board[(new_x, new_y)], (new_x, new_y)))

    # sort to get lowest destination number, return lowest (x,y)
    if result:
        result.sort()
        return result[0][1]

    return None

# the main code goes here
def main():
    # open the output file
    out = open(OutputData, 'w')

    # start the direction generator
    gen_dirn = dirn_gen()

    # construct the dictionary describing the infinite board
    # well, up to MaxNumber marked squares, anyway
    board = {}
    number = 1
    x = 0
    y = 0
    curr_dirn = next(gen_dirn)
    next_dirn = next(gen_dirn)
    for num in range(1, MaxNumber):
        board[(x,y)] = num      # fill position with the number

        # work out next position
        (x, y) = (x+curr_dirn[0], y+curr_dirn[1])   # move one position

        # if next_dirn square is free, swivel
        side_square = (x+next_dirn[0], y+next_dirn[1])
        result = board.get(side_square, None)
        if result is None:
            # swivel
            (curr_dirn, next_dirn) = (next_dirn, next(gen_dirn))

    # create the set of positions that have been visited
    # (0,0) is marked because that's the starting point
    marked = {(0,0)}

    # start the knight at position 1, (0,0)
    (x,y) = (0, 0)
    out.write(f'{x} {y}\n')

    # now keep moving knight
    while True:
        k_move = best_move(x, y, board, marked)
        if not k_move:
            print(f'Stopped at ({x},{y}), position {board[(x,y)]}')
            return 0

        # now move the knight to the new square and update marked
        (x, y) = k_move
        out.write(f'{x} {y}\n')
        marked.add(k_move)

    # close the output file
    out.close()

# to help the befuddled user
def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'h', ['help'])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)

# run the program code
result = main()
sys.exit(result)
