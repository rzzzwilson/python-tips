The Trapped Knight
==================

Mark a square as 1 in an infinite gridded plane and draw the remaining
integers around that initial position in a square spiral.  Place a chess
knight on the square marked with 1.  Move the knight to any legal move 
position, choosing the square with the lowest number as the move
destination.

The knight has a finite number of moves.  After step 2016, square 2084
is visited, after which there are no unvisited squares within one knight
move.

The sequence of numbered squares the knight visits is in 
`The On-line Encyclopedia of Integer Sequences (OEIS) <https://oeis.org/A316667>`_.

A nice Numberphile video with Neil Sloane (editor of the OEIS)
`is here <https://www.youtube.com/watch?v=RGQe8waGJ4w>`_.

Results
-------

The `*.png` image files hold results.  Here's one:

.. image:: sample.png
