import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

# data file path
DataFile = 'data.out'

# lists of start/stop coords
x = []
y = []

# read data into lists of "start" and "stop" coordinate tuples
with open(DataFile) as fd:
    for line in fd:
        (x0, y0) = line.split()
        x.append(int(x0))
        y.append(int(y0))

fig = plt.figure()
ax = fig.add_subplot(111)
plt.axis('equal')

prev_x = x[0]
prev_y = y[0]

c_delta = 1.0 / len(x)
color = (1.0, 0.0, 0.0)
init_color = color
for (xx, yy) in zip(x, y):
    line = Line2D([prev_x, xx], [prev_y, yy], color=color)
    ax.add_line(line)
    prev_x = xx
    prev_y = yy
#    r_delta = color[0] - c_delta
#    g_delta = color[1] + c_delta
#    b_delta = color[2]# + c_delta
    r_delta = color[0]# - c_delta
    g_delta = color[1] + c_delta
    b_delta = color[2]# + c_delta
    color = (r_delta, g_delta, b_delta)

ax.set_xlim(-30, +30)
ax.set_ylim(-30, +30)
plt.annotate('O', (10-0.95,23-0.75), color=init_color)
leg = ax.legend(['The Trapped Knight'], fontsize=15, handlelength=0,
                handletextpad=0, fancybox=True, loc=(0.38, -0.03),
                edgecolor='black', labelcolor='black', shadow=True,
                facecolor='white', )
plt.axis('off')

plt.show()
