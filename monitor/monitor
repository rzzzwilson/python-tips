#!/usr/bin/env python3
"""
Run an executable file in parallel with editing.
* If executable crashes, note the event, traceback, etc
* If the source file updates, kill and rerun the executable.

Usage: monitor [<options>] <prog> [<args>]

where <options> are zero or more of
                  -h         print this help and stop
                  -m <file>  monitor for updates of <file>
                             (default is to monitor <prog>)
and   <prog>    is the executable file to run
and   <args>    are optional arguments

For example, to monitor python execution of a file:

    monitor -m test.py python3 test.py 1 2 3
"""

import os
import sys
import getopt
import time
import signal
import subprocess
import traceback
import termios


def clear_screen():
    os.system('clear')

def main(mon_file, prog_args):
    """Perform the monitoring function."""

    global monitored_pid

    # get initial modification time of the file we must monitor
    try:
        file_tm = os.stat(mon_file).st_mtime
    except FileNotFoundError:
        print(f"Sorry, file '{mon_file}' doesn't exist.")
        return 1

    # start the monitored process, store its PID in "monitored_pid"
    monitored_pid = subprocess.Popen(prog_args)

    while True:
        # check if monitored file has changed
        # sometimes an editor will rename a file, so try more than once
        retry = 3
        while retry:
            try:
                new_tm = os.stat(mon_file).st_mtime
                break
            except FileNotFoundError:
                time.sleep(0.05)
                retry -= 1
                if retry == 0:
                    # the monitored file no longer exists, finish up
                    if monitored_pid:
                        os.kill(monitored_pid.pid, signal.SIGHUP)
                    termios.tcflush(sys.stdin, termios.TCIOFLUSH)
                    print(f"Monitored file '{mon_file}' has disappeared?")
                    exit(1)

        # if file has changed, kill the monitored process and restart
        if new_tm > file_tm:
            # remember new modification time
            file_tm = new_tm

            # kill the monitored process
            os.kill(monitored_pid.pid, signal.SIGHUP)

            # give some indication of the change
            print(f"\n{'*'*60}\nFile '{mon_file}' has changed, restarting...")
            time.sleep(1)
            clear_screen()

            # restart the monitored process
            monitored_pid = subprocess.Popen(prog_args)

        # check if monitored process has died
        if monitored_pid.poll() is not None:
            # get here if monitored process has finished, ie, has an exit status
            termios.tcflush(sys.stdin, termios.TCIOFLUSH)
            input('\nThe monitored process has finished.  Press ENTER to restart.')

            # clear screen and restart process
            clear_screen()
            monitored_pid = subprocess.Popen(prog_args)

        # wait a bit before next loop
        time.sleep(0.1)


# to help the befuddled user
def usage(msg=None):
    if msg:
        print(('*'*80 + '\n%s\n' + '*'*80) % msg)
    print(__doc__)

# our own handler for uncaught exceptions
def excepthook(type, value, tb):
    msg = '\n' + '=' * 80
    msg += '\nUncaught exception:\n'
    msg += ''.join(traceback.format_exception(type, value, tb))
    msg += '=' * 80 + '\n'
    print(msg)
    termios.tcflush(sys.stdin, termios.TCIOFLUSH)
    if monitored_pid:
        os.kill(monitored_pid.pid, signal.SIGINT)

# plug our handler into the python system
sys.excepthook = excepthook

# parse the CLI params
argv = sys.argv[1:]

try:
    (opts, args) = getopt.getopt(argv, 'hm:', ['help', '--monitor'])
except getopt.GetoptError as err:
    usage(err)
    sys.exit(1)

# assume we will monitor args[0]
mon_file = None

for (opt, param) in opts:
    if opt in ['-h', '--help']:
        usage()
        sys.exit(0)
    elif opt in ['-m', '--monitor']:
        mon_file = param

if len(argv) < 1:
    usage()
    sys.exit(1)

if mon_file is None:
    mon_file = argv[0]

# run the program code
try:
    result = main(mon_file, args)
except KeyboardInterrupt:
    print()
    result = 0
sys.exit(result)
