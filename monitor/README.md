# monitor

This is a little python hack that allows you to repeatedly run some code
in a terminal window.  The program monitors the process running the code and
restarts it if it stops.  If the file the monitor is checking changes it kills
and restarts the process running the code.

The aim is to get something like an IDE-like behaviour when editing a python
file.  Start the process running the code you are writing and it restarts the
code when the file changes, ie, when you save the code in the editor.

## Testing

Start editing the file *test_monitor.py* in one terminal window and in another
window run this:

    monitor -m test_monitor.py python3 test_monitor.py

Or if *test_monitor.py* is directly executable, do this in the second window:

    monitor test_monitor.py

When you save in the editor the *monitor* program will restart the python code.
Upon entering the second name *test_monitor.py* will raise an exception, and
the monitor will automatically restart the code when you say so.
