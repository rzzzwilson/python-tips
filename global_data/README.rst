global_data
===========

A method of globally accessing and updating data stored in a file.
The data is persistent.


Simple start
------------

To access global data held in one module, we create a file `common.py` that
contains code to save/restore only user-created objects into a file.

In every module that requires access to common values we do::

    import common
    print(common.title)             # use existing common value
    common.subtitle = 'Sub Title'   # add new common value

Now the code in each module in which we import *common* can use and even update
the attributes of the module, with all changes visible to all modules.


Persistent data
---------------

The above approach is very simple, but doesn't handle the case where we want
the data to be *persistent*, that is, changes made to the data today will be
visible when we run the code tomorrow.

The first change we make is to store the data objects in a JSON file.  We don't
want to save objects that the user isn't interested in, so we don't save module
objects or any object with a name starting with '_'.

If we want to persist the data we have to rewrite the JSON file with the new,
updated data it might contain.  We create a `save_data()` function in the
`common.py` file to do this.  We queue this function to be executed at program
termination.  So common data is automatically saved.


Testing
-------

The files::

    common.py
    main.py
    mod_a.py
    mod_b.py

are used to test the code.  Test by initially doing::

    rm -Rf common.data     # remove persistence file
    python main.py

Running `main.py` again should show that the data was saved from the previous
run.


Drawbacks
---------

Only objects that can be handled by JSON should be put into "common".

User code should not define "common" attribute names that start with '_'.


Updates
-------

Made the data filename come from the environment variable **COMMON_DATA**.

Made the module save function run automatically on module deletion, so there is
no need for the user code to ever call `save_data()`.

Made the code in *common.py* use a lot less global names, not save modules or
names that start with '_', etc.
