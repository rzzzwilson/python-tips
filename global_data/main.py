import common
import mod_a
import mod_b

# create a test var if it doesn't exist
try:
    common.something
except AttributeError:
    common.something = 0

mod_a.print_dict()

print('mod_b.add(5)')
mod_b.add(5)
mod_a.print_dict()
mod_b.print_dict()

print('mod_a.add(4)')
mod_a.add(4)
mod_a.print_dict()
mod_b.print_dict()

print('Creating new key')
common.new = 'new'
mod_a.print_dict()
mod_b.print_dict()

print("Create new global 'test_global'")
common.test_global = 42
mod_b.print_test_global()
print(f'END: {common.something=}, {common.added=}, {common.new=}, {common.test_global=}')
