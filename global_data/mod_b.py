import common

def print_dict():
    print(f'mod_b: {dir(common)=}')

def add(num):
    common.something += num

def print_test_global():
    try:
        print(f'mod_b: {common.test_global=}')
    except NameError:
        print("mod_b: Sorry, 'common.test_global' isn't visible, should be!")
