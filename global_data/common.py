"""
A persistent global module.

Attributes added to this module are visible across all modules importing it.

The common data is saved to a file on exit and restored the next time the code
is run.  The save/restore filename is held in the COMMON_DATA environment
variable.  The default is "common.data" if the environment variable is not
defined.
"""

import os
import sys
import json
import atexit
import types

def _restore():
    """A function to restore the common environment from a save.

    We use a function so we don't have to worry about all the vars
    used here appearing in the globals and having to be weeded out
    when saving.  Call the function after defining it.
    """

    try:
        data_filename = os.environ['COMMON_DATA']
    except KeyError:
        # no envvar, use default filename
        data_filename = 'common.data'

    try:
        with open(data_filename, 'r') as fd:
            common_data = json.loads(fd.read())
    except FileNotFoundError:
        # can't restore, do nothing
        pass
    else:
        # add data from the save to the global namespace
        module = sys.modules[__name__]
        for (key, val) in common_data.items():
            setattr(module, key, val)

    # define the save function here, less visible
    def save_data(filename):
        """Save common data objects to a JSON file.
    
        The file to save to is passed as "filename".
        """
    
        # create list of all the imported modules we must not save
        ignore_names = [key for (key, val) in globals().items()
                        if isinstance(val, types.ModuleType)
                        and not key.startswith('_')]

        # create a dictionary of values to save
        # and write it to a JSON file
        objs = {key:val for (key, val) in globals().items()
                if not key.startswith('_') and key not in ignore_names}
        data_json = json.dumps(objs)
        with open(filename, 'w') as fd:
            fd.write(data_json)

    # queue an automatic save to the data file on program termination
    atexit.register(lambda: save_data(data_filename))

_restore()      # actually perform the restore
