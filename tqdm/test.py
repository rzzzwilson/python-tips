import sys
import tqdm
import httpx

url = "https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz"
with httpx.stream("GET", url) as response:
    total = int(response.headers["Content-Length"])
    with tqdm.tqdm(total=total) as progress:
        for chunk in response.iter_bytes():
            progress.update(len(chunk))
