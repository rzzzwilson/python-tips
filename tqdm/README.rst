This snippet shows using *tqdm* to show internet download progress.

Requirements:

* tqdm  (pip3 install tqdm)
* httpx (pip3 install httpx)
