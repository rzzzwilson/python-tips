# Get user input from a field within a prompt

I ran across this question in the /r/learnpython subreddit:

    https://old.reddit.com/r/learnpython/comments/10n4hnp/input_problem/

Summarized, the OP wanted to accept input from a field **inside** the *input()*
prompt string.  Something like:

    Enter speed: _______ (m/s)

where the "_______" is where the user types their answer.

## One solution

Though the professional and bullet-proof answer is "use curses" or something
similar, it is possible to do something like what the OP wants with basic
python.

Using the *end=* option with *print()* we can leave the cursor on the printed
line.  We need to also use the *flush=True* option with *print()* to ensure
that the text is all printed for reasons to do with buffering.  So this:

    print("Enter speed:         (m/s)", end="", flush=True)
    x = input()
    print(x)

That does leave the cursor on the printed line but at the end, where we don't
want it.  We actually want the cursor to be positioned one space after the
":" in the prompt string.  We add *backspace* characters at the end of the
prompt string to move the cursor left.  If we get the number of backspaces 
correct it does what we want:

    print("Enter speed:         (m/s)\b\b\b\b\b\b\b\b\b\b\b\b\b", end="", flush=True)
    x = input()
    print(x)

## Improving the solution

That's a solution, but if you do a lot of this it is a pain to have to adjust
your prompt string(s) to get the cursor in the right place.  We can write
a function that behaves like the standard *input()* function that does most
of the work for us.

Define that the prompt string passed will contain underscore characters
that define **where** the user should enter data.  So the above example
would use code like this:

    speed = input_field("Enter speed: _______ (m/s)")

The function has to figure out the position within the string where the
cursor should be placed before calling *input()*, ie, the number of
backspaces to append to the prompt string.  That's easy in python:

    def input_field(prompt):
        """Accept input in a field of a prompt.
    
        The "prompt" string contains a field shown by "_" characters.
    
        Returns the string the user typed, just like "input()".
        """
    
        # get number of positions in the input field
        # NOTE: assumes only one field in the prompt string
        field_len = prompt.count('_')

        # get index position of last "_" character
        last_delim = prompt.rindex('_')

        # figure out the length of the text after the field
        tail_len = len(prompt) - last_delim - 1
        
        # print the prompt string with the correct number of backspaces
        # and then return what input() gets
        backspace = "\b"        # because we can't put \b into an f-string
        print(f'{prompt}{backspace*(tail_len+field_len)}', end="", flush=True)
        return input()
    
    x = input_field("Enter speed: _______ (m/s)")

This is a little crude and could be improved a bit.  The file *input_field.py*
has a better version.
