"""
A function to allow the user to enter a value into a delimited
field of the given promptr.
"""


def inputf(prompt, field='_', blank=False, replace=None):
    """Accept input in a field of a prompt.

    The "prompt" string contains a field shown by "_" characters.
    The "field="   option allows the use of characters beside "_",
        "blank="   option allows the field characters to be "blanked"
                   before the prompt is displayed,
        "replace=" option allows the replacement of the field
                   characters with another when printed.

    The field character must exist in the prompt string.

    Note that specifying both "blank=" and "replace=" options will
    have undefined results.

    Returns the string the user typed, just like "input()".

    BUG: Doesn't check if there are two or more contiguous fields
         of the "field" character.
    """

    field_len = prompt.count(field)
    if field_len == 0:
        raise RuntimeError(f"Prompt '{prompt}' doesn't contain field characters '{field}'")

    last_delim = prompt.rindex(field)
    tail_len = len(prompt) - last_delim - 1

    # handle the options
    if replace is not None:
        prompt = prompt.replace(field, replace)
        field = replace
    if blank:
        prompt = prompt.replace(field, " ")
    
    backspace = "\b"
    print(f'{prompt}{backspace*(tail_len+field_len)}', end="", flush=True)
    return input()
