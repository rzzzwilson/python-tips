#!/usr/bin/env python3

"""
Demonstration of calling the "input_field.input()" function.
"""

import input_field

x = input_field.inputf("Enter speed: ....... (m/s)", field=".", blank=True)
print(x)
print()

x = input_field.inputf("Enter speed: ....... (m/s)", field=".", replace="_")
print(x)
print()

x = input_field.inputf("Enter speed: _______ (m/s)", blank=True, replace=".")
print(x)
print()

# expect error from below, no field characters found
print("This test should fail with a RunTimeError exception:")
x = input_field.inputf("Enter speed: _______ (m/s)", field=".", blank=True)
print(x)
