"""
A simple example of running shell commands in threads
and getting the results back.
"""

import time
import queue
import threading
import subprocess

# thread class to run a command
class ExampleThread(threading.Thread):
    def __init__(self, cmd, queue):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.queue = queue

    def run(self):
        # execute the command, queue the result
        completed = subprocess.run(self.cmd, check=True, shell=True,
                                   stdout=subprocess.PIPE)
        self.queue.put((self.cmd, completed.stdout.decode('utf-8'),
                        completed.returncode))

# queue where results are placed
result_queue = queue.Queue()

# define the commands to be run in parallel, then start in a thread
cmds = ["date; ls -l; sleep 1; date",
        "date; sleep 7; date",
        "date; df -h; sleep 3; date",
        "date; hostname; sleep 2; date",
        "date; uname -a; date",
       ]

for cmd in cmds:
    thread = ExampleThread(cmd, result_queue)
    thread.start()

# print results as we get them
while threading.active_count() > 1 or not result_queue.empty():
    while not result_queue.empty():
        (cmd, output, status) = result_queue.get()
        print(f"task >>>>> {cmd} <<<<<\n")
        print(output)
        print("="*60)
    time.sleep(0.1)
