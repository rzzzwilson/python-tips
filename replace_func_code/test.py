"""
Test code to experiment with modifying the executable code of a function.

We create the new code by compiling a function with the new code and
then replacing the __code__ attribute of the target function with code
from the compiled function.
"""

import dis

# the old function whose code will be replaced
def test(x):
    return x


# show code of the old function:
print('Old code in function:')
print('def test(x):')
print('    return x')

# show result of calling the old function
print(f'\nBefore change, {test(3)=}')

print('\nDisassemble old function:')
dis.dis(test)

# show the new code
new_code = 'def xyzzy(x):\n    return x*x'
print(f'\nNew code in function:')
print(f'{new_code}')

# create the new function code string
comp_new_code = compile(new_code, '<string>', 'exec')

# execute the function "def", get function "xyzzy()" defined in "local_env" dict
local_env = {}
exec(new_code, {}, local_env)

# extract __code__ from xyzzy() in local_env dictionary
# and put into the old function
print('\nPut new code into old function')
test.__code__ = local_env['xyzzy'].__code__

# test the new code
print(f'\nAfter change, {test(3)=}')

# disassemble the new test() code
print('\nDisassemble new test():')
dis.dis(test)
